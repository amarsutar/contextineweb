var express = require('express');
var router = express.Router();

/* GET Company page. */
router.get('/product', function(req, res) {
  res.render('product', {
    title: 'Contextine',
    page: 'product'
  });
});

router.get('/product/casemanagement', function(req, res) {
  res.render('casemanagement', {
    title: 'Contextine',
    page: 'product'
  });
});


module.exports = router;
