var express = require('express');
var router = express.Router();

/* GET Company page. */
router.get('/contact', function(req, res) {
  res.render('contact', {
    title: 'Contact',
    page: 'CONTACT'
  });
});

module.exports = router;
