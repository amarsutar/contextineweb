var express = require('express');
var router = express.Router();

/* GET Company page. */
router.get('/company', function(req, res) {
  res.render('company', {
    title: 'Company',
    page: 'Company'
  });
});

module.exports = router;
