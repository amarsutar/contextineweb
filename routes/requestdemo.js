var express = require('express');
var fs = require('fs');
var nodemailer = require("nodemailer");
var crypto = require("crypto-js");
var router = express.Router();
var defaultAuthUser = '';
var defaultAuthPass = ''; 
var defaultEmailServerIP = 'smtp.gmail.com';          
var defaultEmailPort = '587';
var sphrase = "CONGPASSRMNPASURANAPRARMANA15030521TEXTINE";

/* GET Company page. */
router.get('/requestdemo', function(req, res) {
  res.render('requestdemo', {
    title: 'Request Demo',
    page: 'requestdemo',
    data: {name: '', email: '', phone: '', company: '', error: ''}
  });
});

router.post('/requestdemo', function(req, res) {
  console.log(req.body);
  try{
  	sendEmail(req.body, res);
  } catch (e) {
  	 res.redirect('/');
  }
});


var getDefaultUserAuth = function()
{
    var filePath = './settings/default';
    fs.exists(filePath, function(exists) {
      if (exists) {
        var content = fs.readFileSync(filePath);
        if(content !='')
        {         
          var emailSeverSettings = JSON.parse(content);         
          var decrypted = crypto.AES.decrypt(emailSeverSettings.authPass, sphrase);
          defaultAuthUser = 'noreply@contextine.com';
          defaultAuthPass = decrypted.toString(crypto.enc.Utf8);  
          defaultEmailServerIP = 'smtp.gmail.com';          
          defaultEmailPort = '587';
        }
      }
      else
      {
        defaultAuthUser = '';
        defaultAuthPass = '';
      }     
    });   
}
var auth = getDefaultUserAuth();
var sendEmail = function(data, res){
	console.log("Sending email : " + data);
  
	var smtpTransport = nodemailer.createTransport("SMTP",{
	   host:defaultEmailServerIP,
	   port:defaultEmailPort,	   
	   auth: {
		   user: defaultAuthUser,
		   pass: defaultAuthPass
	   }
	});

	var mailOptions = {  //email options
	   from: defaultAuthUser, // sender address.  Must be the same as authenticated user if using GMail.
	   to: "info@contextine.com", // receiver
	   subject: "Demo request from " + data.name + '(' + data.email +')', // subject   
	   text: 'Name: ' + data.name + '\n\nEmail: ' + data.email + '\n\nPhone: ' + data.phone + '\n\nCompany: ' + data.company // body	   
	}
	
	smtpTransport.sendMail(mailOptions, function(error, response){  //callback
	   if(error){
		   console.log(error);

			res.render('requestdemo', {
			    title: 'Request Demo',
			    page: 'requestdemo',
			    data:{name: data.name, email: data.email, phone: data.phone, company: data.company, error: 'Unable to send an Email! Please try again later!'}
			});

	   }else{
		   console.log("Message sent: " + response.message);	
		   res.redirect('/');
	   }
	   
	   smtpTransport.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
	});
}

module.exports = router;
